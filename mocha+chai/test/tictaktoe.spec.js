describe('Tic-Tak-Toe', function() {
	/*describe('#getGridSize', function(){
		it('returns size of grid', function(){
			var 
				grid = [
					[0, 0, 0],
					[0, 0, 0], 
					[0, 0, 0]
				], 
				tikTakToe = new TicTakToe(), 
				size;

			tikTakToe.setGrid(grid);
			size = tikTakToe.getGridSize();
			expect(size).to.equal(9);
		});
	});*/
	describe('#move', function(){
		/*it('returns if grid changed', function(){
			var 
				grid = [
					[0, 0, 0],
					[0, 0, 0], 
					[0, 0, 0]
				],
				tikTakToe = new TicTakToe(),
				size,
				gr1,
				gr2;

			tikTakToe.setGrid(grid);
			gr1 = tikTakToe.getGrid();
			tikTakToe.move('x', {r: 1, c: 1});
			gr2 = tikTakToe.getGrid();
			expect(gr1).to.not.equal(gr2);
		});*/
		it('returns if x is move', function(){
			var 
				grid = [
					[0, 0, 0],
					[0, 0, 0], 
					[0, 0, 0]
				],
				tikTakToe = new TicTakToe();

			tikTakToe.setGrid(grid);
			tikTakToe.move('x', {r: 1, c: 1});
			expect(tikTakToe.getGrid()[1][1]).to.equal(1);
		});
		it('returns if 0 is move', function(){
			var 
				grid = [
					[0, 0, 0],
					[0, 0, 0], 
					[0, 0, 0]
				],
				tikTakToe = new TicTakToe();

			tikTakToe.setGrid(grid);
			tikTakToe.move('o', {r: 1, c: 1});
			expect(tikTakToe.getGrid()[1][1]).to.equal(2);
		});
		it('returns ex if game ended', function(){
			var 
				grid = [
					[1, 2, 1],
					[1, 2, 2], 
					[2, 1, 1]
				],
				tikTakToe = new TicTakToe();

			tikTakToe.setGrid(grid);
			expect(tikTakToe.move('x', {r: 1, c: 1})).to.throw(Error);
		});
		it('returns ex if cell rewrited', function(){
			var 
				grid = [
					[0, 0, 0],
					[0, 0, 0], 
					[0, 0, 1]
				],
				tikTakToe = new TicTakToe();

			tikTakToe.setGrid(grid);
			tikTakToe.move('x', {r: 0, c: 0});
			expect(tikTakToe.move('o', {r: 0, c: 0})).to.throw(Error);
		});
		it('returns ex if X tries to play the second consecutive', function(){
			var 
				grid = [
					[1, 0, 0],
					[0, 0, 0], 
					[0, 0, 1]
				],
				tikTakToe = new TicTakToe(), 
				size;

			tikTakToe.setGrid(grid);
			tikTakToe.move('x', {r: 0, c: 0});
			expect(tikTakToe.move('x', {r: 1, c: 0})).to.throw(Error);
		});
		it('returns ex if O tries to play the second consecutive', function(){
			var 
				grid = [
					[1, 0, 0],
					[0, 0, 0], 
					[0, 0, 1]
				],
				tikTakToe = new TicTakToe(), 
				size;

			tikTakToe.setGrid(grid);
			tikTakToe.move('o', {r: 0, c: 0});
			expect(tikTakToe.move('o', {r: 1, c: 0})).to.throw(Error);
		});
	});
	describe('#check', function(){
		it('returns if player wins', function(){
			var 
				grid = [
					[1, 0, 0],
					[0, 1, 0], 
					[0, 0, 1]
				],
				tikTakToe = new TicTakToe();

			tikTakToe.setGrid(grid);
			var isWin = tikTakToe.check();
			expect(isWin).to.be.true;
		});
		it('returns if not winners', function(){
			var 
				grid = [
					[1, 2, 1],
					[1, 2, 2], 
					[2, 1, 1]
				],
				tikTakToe = new TicTakToe();

			tikTakToe.setGrid(grid);
			var isWin = tikTakToe.check();
			expect(isWin).to.be.false;
		});
	});
});