function TicTakToe(){
	this.markers = {
		x: 1,
		o: 2
	},
	this.last = 0;
	this.end = false;
}

TicTakToe.prototype.setGrid = function(grid){
	this.grid = grid;
};

TicTakToe.prototype.getGrid = function(){
	return this.grid;
};

TicTakToe.prototype.move = function(type, position){

	var marker = this.markers[type];

	if(this.last === marker){
		return new Error();
	}

	if(this.grid[position.r][position.c] === marker){
		return new Error();
	}

	this.last = marker;

	return this.grid[position.r][position.c] = marker;

	
};

TicTakToe.prototype.check = function(){
	return this.end;
};
